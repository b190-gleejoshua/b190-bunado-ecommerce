const express =require("express");
const mongoose = require("mongoose");
// allows us to control the app's Cross-origin Resources sharing settings
const cors = require("cors");

const userRoutes = require("./routes/userRoutes.js");
const courseRoutes = require("./routes/courseRoutes.js");
const productRoutes = require("./routes/productRoutes.js");
const orderRoutes = require("./routes/orderRoutes.js");
const app = express();

// MongoDB Connection
mongoose.connect("mongodb+srv://budz956:admin123@wdc028-course-booking.q2syofq.mongodb.net/b190-bunado-ecommerce?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", ()=> console.log("We're connected to the database"));

// allows all resources to access our backend application
app.use(cors());

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use("/users", userRoutes);
app.use("/courses", courseRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes)

/*
	Heroku deployment
		Procfile - needed 

	"https://serene-garden-45385.herokuapp.com/" - heroku pathway 
*/




// process.env.PORT handles the environment of the hosting websites shoul app be hosted in a website such as Heroku
app.listen(process.env.PORT || 4000,()=> {console.log(`API now online at port ${process.env.Port || 4000}`)})