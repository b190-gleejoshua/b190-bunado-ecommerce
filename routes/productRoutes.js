const express = require("express");
const router = express.Router();

const productController = require("../controllers/productController.js")
const auth = require("../auth.js");


// route for creating a creating product
router.post("/", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin === false) {
		res.send(false);
	} else {
		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	}
});


// route for getting all products
router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController=>res.send(resultFromController))
})


// route for getting ACTIVE products
router.get("/", (req, res)=>{
	productController.getAllActive().then(resultFromController=>res.send(resultFromController))
});

// route for getting products Id
router.get("/:productId", (req, res) =>{
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
});

// route for updating existing product
router.put("/:productId", auth.verify, (req, res)=>{
	if(auth.decode(req.headers.authorization).isAdmin === false){
		res.send(false);
	}else{
		productController.updateProduct(req.params, req.body).then(resultFromController =>res.send(resultFromController))
	}
	
});

// archiving products
router.put("/:productId/archive", auth.verify, (req, res)=>{
	if(auth.decode(req.headers.authorization).isAdmin === false){
		res.send(false);
	}else{

		productController.archiveProduct(req.params, req.body).then(resultFromController =>res.send(resultFromController))
	}
});
module.exports = router