const express = require("express");
const router = express.Router();

const userController = require("../controllers/userController.js")
const auth = require("../auth.js");
const Product = require("../models/Product.js");
const Order = require("../models/Order.js");

// check if email/user already exists
router.post("/checkEmail", (req, res)=>{
	userController.checkEmailExists(req.body).then(resultFromController=>res.send(resultFromController));
})

//route for resgistering a user
router.post("/register", (req, res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController));
})

// route for user authentication
router.post("/login", (req, res)=>{
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// route for restreiving user details
router.get("/details", auth.verify, (req, res) => {
	// 
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});

// route for user enrollment
router.post("/enroll", auth.verify, (req, res)=>{
	if(auth.decode(req.headers.authorization).isAdmin === true) {
		res.send(false);
	} else {
		let data = {
			userId: auth.decode(req.headers.authorization).id,
			courseId: req.body.courseId
		}
		userController.enroll(data).then(resultFromController=>res.send(resultFromController))
	}

	
});


// route for creating order
router.post("/createOrder", auth.verify, (req, res)=>{
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		orderId: req.body.orderId,
		productId: req.body.productId,
		orderQuantity: req.body.orderQuantity,
		totalAmount: req.body.totalAmount
	}
	userController.createOrder(data).then(resultFromController=>res.send(resultFromController))
});

// Route for setting user as admin
router.put("/:userId/setAsAdmin", auth.verify, (req, res)=>{
	if(auth.decode(req.headers.authorization).isAdmin === false){
		res.send(false);
	}else{

		userController.setAsAdmin(req.params, req.body).then(resultFromController =>res.send(resultFromController))
	}
});






module.exports = router;