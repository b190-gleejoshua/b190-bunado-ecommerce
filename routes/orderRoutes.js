const express = require("express");
const router = express.Router();

const orderController = require("../controllers/orderController.js")
const auth = require("../auth.js");


// route for adding order
router.post("/", auth.verify, (req, res) => {
	orderController.addOrder(req.body).then(resultFromController => res.send(resultFromController));
});

	


router.get("/all", auth.verify, (req, res)=>{
	if(auth.decode(req.headers.authorization).isAdmin === false) {
		res.send(false);
	}else{
		orderController.getAllOrders().then(resultFromController=> res.send(resultFromController));
	}
	
})

module.exports = router;