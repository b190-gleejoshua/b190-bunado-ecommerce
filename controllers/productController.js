const Product = require("../models/Product.js");
const auth = require("../auth.js");



module.exports.addProduct = (reqBody)=>{
	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price:reqBody.price

	})
	return newProduct.save().then((product, error) =>{
		if (error) {
			return false;
		}else{
			return true;
		}
	})
}


module.exports.getAllProducts = ()=>{
	return Product.find({ }).then(result => {
		return result;
	});
};

// retreive all active courses

module.exports.getAllActive = ()=>{
	return Product.find({isActive: true}).then(result =>{
		return result
		
	})
};

// route for retrieving a specific course

module.exports.getProduct = (reqParams) =>{
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
};



// update a course

module.exports.updateProduct = (reqParams, reqBody) =>{
	let updateProduct = {
		name:reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	// findByIdAndUpdate - its purpose os to find a specific database (first Parmameter) and update it using the information from the request body (second Parameter)
	return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((result, error)=>{
		if (error){
			return false;
		}else{
			return true;
		}


	})

	
}

module.exports.archiveProduct = (reqParams, reqBody) =>{
	let updateActiveField ={
		isActive: reqBody.isActive
	}
	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((result, error)=>{
		if(error){
			return false;
		}else{
			return true;
		}
	})
}
