const User = require("../models/User.js");
const Course = require("../models/Course.js");
const Product = require("../models/Product.js");
const Order = require("../models/Order.js");
const auth = require("../auth.js"); 

const bcrypt = require("bcrypt");


// check if user is already registered
module.exports.checkEmailExists = ( reqBody ) => {
	return User.find( { email: reqBody.email } ).then( result => {
		
		if (result.length > 0){
			return true;
		}else{
		
			return false;
		}
	} )
};



// register user
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user, error) =>{
		if (error) {
			return false;
		}else{
			return true;
		}
	})
};


// login user
module.exports.loginUser = ( reqBody ) => {
	return User.findOne( { email: reqBody.email } ).then(result =>{
		
		if(result === null){
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result) }
			}else{
				return false;
			}
		}
	} )
} ;

// get user details
module.exports.getProfile = (data) => {
	console.log(data);
	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	});
};
// enroll user
module.exports.enroll = async (data) =>{
	let isUserUpdated = await User.findById(data.userId).then(user=>{user.enrollments.push({courseId: data.courseId})

		return user.save().then((user,error)=>{
			if(error){
				return false;
			}else{
				return true;
			}
		})
	})
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({userId: data.userId})

		return course.save().then((course, error)=>{
			if(error){
				return false;
			}else{
				return true;
			}
		})
	})


	if(isUserUpdated && isCourseUpdated){
		return true
	}else{
		return false
	}

}

// creating order	

module.exports.createOrder = async (data) =>{
	let isUserUpdated = await User.findById(data.userId).then(user=>{
		for(i = 0; i<4; i++)
			user.orders.push({
			orderId: data.orderId,
			productId: data.productId,
			orderQuantity: data.orderQuantity,

		})

		return user.save().then((user,error)=>{
			if(error){
				return false;
			}else{
				return true;
			}
		})

	})

	let isOrderUpdated = await Order.findById(data.orderId).then(order => {
		for(i = 0; i<3; i++)
			order.orderDetails.push({
			userId: data.userId,
			productId: data.productId,
			totalAmount: data.totalAmount

		})


		return order.save().then((order, error)=>{
			if(error){
				return false;
			}else{
				return true;
			}
		})
	})
	

	if(isUserUpdated){
		return true
	}else{
		return false
	}

}


// setting non admin user to admin
module.exports.setAsAdmin = (reqParams, reqBody) =>{
	let updateAdminField ={
		isAdmin: true
	}
	return User.findByIdAndUpdate(reqParams.userId, updateAdminField).then((result, error)=>{
		if(error){
			return false;
		}else{
			return true;
		}
	})
}


