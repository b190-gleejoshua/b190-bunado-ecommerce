const Order = require("../models/Order.js");
const auth = require("../auth.js");


// codes for adding an order
module.exports.addOrder = (reqBody)=>{
	let newOrder = new Order({
		name: reqBody.name
	})
	return newOrder.save().then((oder, error) =>{
		if (error) {
			return false;
		}else{
			return true;
		}
	})
};


// code for getting all orders(Admin Only)

module.exports.getAllOrders = ()=>{
	return Order.find({ }).then(result =>{
		return result
	});
};
